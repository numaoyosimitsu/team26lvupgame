﻿using System.Diagnostics; 
using System.IO; 
using DxLibDLL;

namespace LVupGame
{
    
    public class Map
    {
        public const int None = -1;   
        public const int Wall = 0;  
        public const int Needle = 1;   
        public const int Brick = 2;  
        public const int Floor = 3;  

        public const int Width = 30; 
        public const int Height = 20;  
        public const int CellSize = 32; 

        PlayScene playScene;
        int[,] terrain; 
        
        public Map(PlayScene playScene, string stageName)
        {
            this.playScene = playScene;
            LoadTerrain("Map/" + stageName + ".csv");
            LoadObjects("Map/" + stageName + "_object.csv");
        }

        void LoadTerrain(string filePath)
        {
            terrain = new int[Width, Height]; 

            string[] lines = File.ReadAllLines(filePath); 
            
            Debug.Assert(lines.Length == Height, filePath + "の高さが不正です：" + lines.Length);

            for (int y = 0; y < Height; y++)
            {
                string[] splitted = lines[y].Split(new char[] { ',' });
                
                Debug.Assert(splitted.Length == Width, filePath + "の" + y + "行目の列数が不正です:" + splitted.Length);

                for (int x = 0; x < Width; x++)
                {
                    terrain[x, y] = int.Parse(splitted[x]);
                }
            }
        }

        //オブジェクト(敵キャラクターなど)のCSVを読み込む
        void LoadObjects(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            Debug.Assert(lines.Length == Height, filePath + "の高さが不正です：" + lines.Length);
            for (int y = 0; y < Height; y++)
            {
                string[] splitted = lines[y].Split(new char[] { ',' });//行数の検証
                Debug.Assert(splitted.Length == Width, filePath + "の" + y + "行列の列数が不正です:" + splitted.Length);
                for (int x = 0; x < Width; x++)
                {
                    int id = int.Parse(splitted[x]);
                    if (id == -1) continue;
                    SpawnObject(x, y, id);
                }
            }
        }

        void SpawnObject(int mapX, int mapY, int objectID)
        {
            // 生成位置
            float spawnX = mapX * CellSize;
            float spawnY = mapY * CellSize;
            if (objectID == 0)
            {
                GameObject p = playScene.gameObjects.Find(c => c.ID == "Player");
                if (objectID == 0)
                {
                    Player player = new Player(playScene, spawnX, spawnY);
                    playScene.gameObjects.Add(player);
                    playScene.player = player;
                }
            }
            else if (objectID == 1)
            {
                playScene.gameObjects.Add(new Slime(playScene, spawnX, spawnY));
            }
            else if (objectID == 2)
            {
                playScene.gameObjects.Add(new Oak(playScene, spawnX, spawnY));
            }
            else if (objectID == 3)
            {
                playScene.gameObjects.Add(new Eye(playScene, spawnX, spawnY));
            }
            else
            {
                Debug.Assert(false, "オブジェクトID" + objectID + "番の生成処理は未実装です。");
            }
        }

        public void DrawTerrain()
        {
            int left = (int)(Camera.x / CellSize);
            int top = (int)(Camera.y / CellSize);
            int right = (int)((Camera.x + Screen.Width - 1) / CellSize);
            int bottom = (int)((Camera.y + Screen.Height - 1) / CellSize);

            if (left < 0) left = 0;
            if (top < 0) top = 0;
            if (right >= Width) right = Width - 1;
            if (bottom >= Height) bottom = Height - 1;

            for (int y = top; y <= bottom; y++)
            {
                for (int x = left; x <= right; x++)
                {
                    int id = terrain[x, y];

                    if (id == None) continue;

                    Camera.DrawGraph(x * CellSize, y * CellSize, Image.mapchip[id]);
                }
            }
        }
        public int GetTerrain(float worldX, float worldY)
        {
            int mapX = (int)(worldX / CellSize);
            int mapY = (int)(worldY / CellSize);
            
            if (mapX < 0 || mapX >= Width || mapY < 0 || mapY >= Height)
                return None;

            return terrain[mapX, mapY]; 
        }
        
        public bool IsWall(float worldX, float worldY)
        {
            int terrainID = GetTerrain(worldX, worldY); 

            return terrainID == Wall;
        }
    }
}