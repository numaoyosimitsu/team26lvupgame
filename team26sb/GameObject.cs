﻿using DxLibDLL;
using MyLib;


namespace LVupGame
{
    public abstract class GameObject
    {
        public float x;
        public float y;
        public bool isDead = false;
        public string ID;

        public float life = 0;
        public int Exp = 0;

        protected PlayScene playScene;
        protected int imageWidth;
        protected int imageHeight;
        protected int hitboxOffsetLeft = 0;
        protected int hitboxOffsetRight = 0;
        protected int hitboxOffsetTop = 0;
        protected int hitboxOffsetBottom = 0;

        public GameObject(PlayScene playScene)
        {
            this.playScene = playScene;
        }

        public virtual float GetLeft()
        {
            return x + hitboxOffsetLeft;
        }

        public virtual void SetLeft(float left)
        {
            x = left - hitboxOffsetLeft;
        }

        public virtual float GetRight()
        {
            return x + imageWidth - hitboxOffsetRight;
        }

        public virtual void SetRight(float right)
        {
            x = right + hitboxOffsetRight - imageWidth;
        }

        public virtual float GetTop()
        {
            return y + hitboxOffsetTop;
        }

        public virtual void SetTop(float top)
        {
            y = top - hitboxOffsetTop;
        }

        public virtual float GetBottom()
        {
            return y + imageHeight - hitboxOffsetBottom;
        }

        public virtual void SetBottom(float bottom)
        {
            y = bottom + hitboxOffsetBottom - imageHeight;
        }

        public abstract void Update();

        public abstract void Draw();

        public void DrawHitBox()
        {
            Camera.DrawLineBox(
                GetLeft() + 0.5f,
                GetTop() + 0.5f,
                GetRight() + 0.5f,
                GetBottom() + 0.5f,
                DX.GetColor(255, 0, 0));

        }
        public abstract void OnCollision(GameObject other);

        public virtual bool IsVisible()
        {
            return MyMath.RectRectIntersect(
                x, y, x + imageWidth, y + imageHeight,
                Camera.x, Camera.y, Camera.x + Screen.Width, Camera.y + Screen.Height);
        }
    }
}