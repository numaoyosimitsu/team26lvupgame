﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;//Listを使うのに必要
using MyLib;
using DxLibDLL;



namespace LVupGame
{
   public class Srasheffect:GameObject
    {
        Player player;
       
        const float MoveSpeed = 10;
        
        int counter=-1 ; 
        int imageIndex ;
     public  Direction PlayerD;
        double angle = 0;
        public Srasheffect(PlayScene playScene, float x, float y, Direction direction) : base(playScene)
        {
            PlayerD = playScene.NearPlayer;
            
            this.x = x;
            this.y = y;
           
           
        }

        public override void Update()
        {

            //PlayerD = playScene.NearPlayer;

            counter++;
            imageIndex = counter / 4;



            if (PlayerD == Direction.Up)
            {
                angle = 0;
            }
            else if (PlayerD == Direction.Right)
            {
                angle = Math.PI * 0.5f;

            }
            else if (PlayerD == Direction.Left)
            {
                angle = Math.PI * 1.5f;
            }
            else if (PlayerD == Direction.Down)
            {
                angle = Math.PI ;
            }
            if (imageIndex >= 5)
            {
                    isDead = true;
                    imageIndex = 1;
                    counter = 1;
            }
            }
        public override void OnCollision(GameObject other)
        {

        }
        public override void Draw()
        {
            // Image.explosionは配列であり、
            // 分割して読み込んだ絵のハンドルが各要素に格納されているので、
            // 要素番号を指定してハンドルを取り出して使う。
            //Camera.DrawGraph(x,y,Image.Sword[imageIndex]);
            //if (PlayerD == Direction.Up) Camera.DrawGraph(x, y,Image.SrashEffect[(imageIndex)]);
            //if (PlayerD == Direction.Down) Camera.DrawGraph(x, y, Image.SrashEffect[(imageIndex)]);
            Camera.DrawRotaGraph(x, y,angle, Image.Sword[(imageIndex)]);

            // DX.DrawRotaGraphF(x, y, 1, 0, Image.Sword[imageIndex]);
        }
        
    }
}
