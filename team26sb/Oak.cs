﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class Oak : GameObject
    {
        enum State
        {
            Normal,
            Swoon,
        }


        State state = State.Normal;
        const int MutekiJikan = 120;
        int swoonTime = 120;
        float count = 0;
        float PlayerPower = 0;


        int mutekiTimer = 0;


        public Oak(PlayScene playScene, float x, float y) : base(playScene)
        {
            this.x = x;
            this.y = y;

            life = 20;
            Exp = 1;

            imageWidth = 32;
            imageHeight = 32;
            hitboxOffsetLeft = 0;
            hitboxOffsetRight = 0;
            hitboxOffsetTop = 0;
            hitboxOffsetBottom = 0;

            playScene.enemyNum += 1;
            PlayerPower = playScene.Playrepower;
        }


        public override void Update()
        {
            if (state == State.Normal)
            {
                float angleToPlayer = MyMath.PointToPointAngle(x, y, playScene.player.x, playScene.player.y);
                float speed = 1.0f;
                x += (float)Math.Cos(angleToPlayer) * speed;
                y += (float)Math.Sin(angleToPlayer) * speed;
            }

            if (state == State.Swoon)
            {
                swoonTime--; // タイマー減少

                if (swoonTime <= 0) // タイマーが0になったら
                {
                    state = State.Normal;
                    swoonTime = 120;
                }
            }

        }

        public override void Draw()
        {

            if (mutekiTimer <= 0 || mutekiTimer % 2 == 0)
            {
                Camera.DrawGraph(x, y, Image.Oak);
            }
        }

        public override void OnCollision(GameObject other)
        {
            if (other is Playersrash) // 相手がPlayerShotクラスなら
            {
                TakeDamage();
            }
            if (state == State.Normal)
            {
                if (other is Boomerang) // 相手がPlayerShotクラスなら
                {
                    state = State.Swoon;
                }
            }
            else if (state == State.Swoon)
            {
                TakeDamage();
            }
        }
       





        public void EnemyDie()
        {
            playScene.playerEXP += 1;
            playScene.enemyNum -= 1;
        }
        void TakeDamage()
        {
            life -= PlayerPower;

            if (life <= 0)
            {
                isDead = true;
                EnemyDie();
            }
            else
            {
                mutekiTimer = MutekiJikan;
            }
        }
    }

}
