﻿using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class Player : GameObject
    {

        public enum State
        {
            Walk,
            Dead,
        }

        const float WalkSpeed = 3f;
        const int MutekiJikan = 120;
        float Lv = 1;
        float NExp = 0;
        public float Power = 1f;
        float vx = 0;
        float vy = 0;
        int counter = 2;
        int Attakcounter = 0;
        int Walkcounter=0;
        float BoomerancoolTimer = 0;

        Playersrash Playersrash;
        int mutekiTimer = 0;
        public float collisionRadius = 8f;
       public Direction player = Direction.Right;
        State state = State.Walk;

        public Player(PlayScene playScene, float x, float y) : base(playScene)
        {
            this.x = x;
            this.y = y;

            life = 100;

            imageWidth = 32;
            imageHeight = 32;
            hitboxOffsetLeft = 10;
            hitboxOffsetRight = 10;
            hitboxOffsetTop = 5;
            hitboxOffsetBottom = 5;
        }

        public override void Update()
        {
            if (Lv * 2 <= playScene.playerEXP)
            {
                Lv += 1f;
                Power += 0.5f;
                life = 3.0f;
                playScene.playerEXP = 0;
            }
            // 入力処理
            HandleInput();
            
            MoveX();
            MoveY();

            BoomerancoolTimer--;

            if (Input.GetButtonDown(DX.PAD_INPUT_1))
            {
                if (BoomerancoolTimer <= 0)
                {
                    float shotX;
                    if (player == Direction.Left) shotX = GetLeft() - 16;
                    else shotX = GetRight();
                    // ショットのy座標
                    float shotY;
                    if (player == Direction.Down) shotY = GetBottom();
                    else shotY = GetTop();

                    playScene.gameObjects.Add(new Boomerang(playScene, shotX, shotY, player));

                    BoomerancoolTimer = 60f * 2f;
                }
            }

            if (Input.GetButtonDown(DX.PAD_INPUT_3))
            {
                float shotX = x;
                float shotY = y - 25;
                if (player == Direction.Up) shotX = x;
                else if (player == Direction.Down) shotX = x;
                if (player == Direction.Up) shotY = y - 30;
                else if (player == Direction.Down) shotY = y + 30;
                if (player == Direction.Left) shotY = y;
                else if (player == Direction.Right) shotY = y;
                if (player == Direction.Left) shotX = x - 30;
                else if (player == Direction.Right) shotX = x + 30;

                playScene.gameObjects.Add(new Shield(playScene, shotX, shotY, player));
            }

            if (Input.GetButtonDown(DX.PAD_INPUT_2))
            {
                counter--;
                // ショットのx座標を求める 
                float shotX = x;
                float shotY = y - 25;
                float srashX = x;
                float srashY = x;
                float shotXleft = x + 20;
                float shotYleft = y;
                float shotXright = x + 20;
                float shotYright = y;
                if (player == Direction.Up) shotX = x + 5;
                else if (player == Direction.Down) shotX = x + 5;
                if (player == Direction.Up) shotY = y - 25;
                else if (player == Direction.Down) shotY = y + 35;
                if (player == Direction.Left) shotY = y + 2;
                else if (player == Direction.Right) shotY = y + 2;
                if (player == Direction.Left) shotX = x - 20;
                else if (player == Direction.Right) shotX = x + 30;
                //左サイドの判定
                if (player == Direction.Up) shotXleft = x - 15;
                else if (player == Direction.Down) shotXleft = x - 15;
                if (player == Direction.Up) shotYleft = y - 15;
                else if (player == Direction.Down) shotYleft = y + 25;
                if (player == Direction.Left) shotXleft = x - 10;
                else if (player == Direction.Right) shotXleft = x + 20;
                if (player == Direction.Left) shotYleft = y + 20;
                else if (player == Direction.Right) shotYleft = y + 20;
                //右サイドの判定
                if (player == Direction.Up) shotXright = x + 25;
                else if (player == Direction.Down) shotXright = x + 25;
                if (player == Direction.Up) shotYright = y - 15;
                else if (player == Direction.Down) shotYright = y + 25;
                if (player == Direction.Left) shotXright = x - 10;
                else if (player == Direction.Right) shotXright = x + 20;
                if (player == Direction.Left) shotYright = y - 15;
                else if (player == Direction.Right) shotYright = y - 15;
                //斬撃エフェクトの調整
                if (player == Direction.Up) srashX = x+15;
                else if (player == Direction.Down) srashX = x+20;
                if (player == Direction.Up) srashY = y - 20;
                else if (player == Direction.Down) srashY = y +50;
                if (player == Direction.Left) srashX = x - 20;
                else if (player == Direction.Right) srashX = x + 50;
                if (player == Direction.Left) srashY = y+20;
                else if (player == Direction.Right) srashY = y +20;
                if (counter <= 0)
                {
                    // 発射 
                    playScene.gameObjects.Add(new Playersrash(playScene, shotX, shotY, player));
                    playScene.gameObjects.Add(new Playersrash(playScene, shotXleft, shotYleft, player));
                    playScene.gameObjects.Add(new Playersrash(playScene, shotXright, shotYright, player));
                    counter += 2;
                    Sound.Play(Sound.Ken);
                    playScene.gameObjects.Add(new Srasheffect(playScene,srashX, srashY, player));
                  // if(vy<=1) playScene.gameObjects.Add(new Srasheffect(playScene, srashX, srashY-30, player));
                  // else if(vy>=-1) playScene.gameObjects.Add(new Srasheffect(playScene, srashX, srashY+30, player));
                  //else if (vx <= -1) playScene.gameObjects.Add(new Srasheffect(playScene, srashX, srashY + 30, player));
                  //  else if (vx >= 1) playScene.gameObjects.Add(new Srasheffect(playScene, srashX - 30, srashY , player));
                }
            }

        }

        void HandleInput()
        {
            if (vx != 0 && vy != 0)
            {
                vx /= MyMath.Sqrt2;
                vy /= MyMath.Sqrt2;
            }


            if (Input.GetButton(DX.PAD_INPUT_LEFT))
            {
                vx = -WalkSpeed;
                player = Direction.Left;
            }
            else if (Input.GetButton(DX.PAD_INPUT_RIGHT))
            {
                vx = WalkSpeed;
                player = Direction.Right;
            }
            else if (Input.GetButton(DX.PAD_INPUT_UP))
            {
                vy = -WalkSpeed;
                player = Direction.Up;
            }
            else if (Input.GetButton(DX.PAD_INPUT_DOWN))
            {
                vy = WalkSpeed;
                player = Direction.Down;
            }
            else
            {
                vx = 0;
                vy = 0;
            }
            


        }

        void MoveX()
        {
            x += vx;

            float left = GetLeft();
            float right = GetRight() - 0.01f;
            float top = GetTop();
            float middle = top + 32;
            float bottom = GetBottom() - 0.01f;

            if (playScene.map.IsWall(left, top) ||
                playScene.map.IsWall(left, middle) ||
                playScene.map.IsWall(left, bottom))
            {
                float wallRight = left - left % Map.CellSize + Map.CellSize;
                SetLeft(wallRight);
            }
            else if (
                playScene.map.IsWall(right, top) ||
                playScene.map.IsWall(right, middle) ||
                playScene.map.IsWall(right, bottom))
            {
                float wallLeft = right - right % Map.CellSize;
                SetRight(wallLeft);
            }
        }

        void MoveY()
        {
            y += vy;

            float left = GetLeft();
            float right = GetRight() - 0.01f;
            float top = GetTop();
            float middle = top + 32;
            float bottom = GetBottom() - 0.01f;

            if (playScene.map.IsWall(left, top) ||
                 playScene.map.IsWall(right, middle) ||
                playScene.map.IsWall(right, top))
            {
                float wallBottom = top - top % Map.CellSize + Map.CellSize;
                SetTop(wallBottom);
            }
            else if (
                playScene.map.IsWall(left, bottom) ||
                playScene.map.IsWall(right, bottom))
            {

            }

            mutekiTimer--;

        }

        public override void Draw()
        {
           
            if (mutekiTimer <= 0 || mutekiTimer % 2 == 0)
            {
                if (vx != 0 || vy != 0) Walkcounter++;
                if (player == Direction.Up) Camera.DrawGraph(x, y, Image.Player[9 + (Walkcounter / 9 % 3)]);
                if (player == Direction.Down) Camera.DrawGraph(x, y, Image.Player[0 + (Walkcounter / 9 % 3)]);
                if (player == Direction.Right) Camera.DrawGraph(x, y, Image.Player[6 + (Walkcounter / 9 % 3)]);
                if (player == Direction.Left) Camera.DrawGraph(x, y, Image.Player[3 + (Walkcounter / 9 % 3)]);
            }
                
            DX.DrawString(0, 0, "LIFE" + " " + life, DX.GetColor(255, 255, 255));
        }

        //敵弾当たり判定
        public void OnCollisionEnemyBullet(EnemyBullet enemyBullet)
        {
            if (mutekiTimer <= 0)
            {
                TakeDamage();
            }
        }
       
        public override void OnCollision(GameObject other)
        {
            if (other is Boomerang)
            {
                return;
            }
            if(other is Shield)
            {
                return;
            }

            if (other is Playersrash)
            {
                return;

            }
            if (other is Srasheffect)
            {
                return;

            }
            else if (mutekiTimer <= 0)
            {
                TakeDamage();

            }
        }

        // ダメージ処理
        void TakeDamage()
        {
            life -= 1;
            Sound.Play(Sound.damage);
            if (life <= 0)
            {
                isDead = true;
                state = State.Dead;
                Die();
            }
            else
            {

                mutekiTimer = MutekiJikan;
            }
        }
        public void Die()
        {
            if (isDead == true)
            {
                playScene.scenestate = PlayScene.State.PlayerDied;
            }
        }
    }
}////////