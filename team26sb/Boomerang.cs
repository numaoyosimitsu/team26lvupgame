﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class Boomerang : GameObject
    {
        enum State
        {
            Go,
            Back, 
        }

        State state = State.Go;
        

        float vz;

        float cooltimer;
        float angle = 0;
        float counter = 0;
        float anglecount = 0;

        float radius_x = 2.5f;
        float radius_y = 4f;

       
        // プレイヤーの弾
        public Boomerang(PlayScene playScene, float x, float y, Direction direction) : base(playScene)
        {


            this.x = x;
            this.y = y;


            imageWidth = 16;
            imageHeight = 16;
            hitboxOffsetLeft = 0;
            hitboxOffsetRight = 0;
            hitboxOffsetTop = 3;
            hitboxOffsetBottom = 3;

            // 向き
            if (direction == Direction.Right) angle = 90;
            if (direction == Direction.Left) angle = 270f;
            if (direction == Direction.Up) angle = 360f;
            if (direction == Direction.Down) angle = 180f;

        }

        public override void Update()
        {
            
            if (state == State.Go)
            {
                float CenterX = x;
                float CenterY = y;

                CenterX += vz;
                CenterY += vz;

                float add_x = radius_x * (float)Math.Cos(Math.PI / 180 * angle) + CenterX;
                float add_y = radius_y * (float)Math.Sin(Math.PI / 180 * angle) + CenterY;

                x = add_x;
                y = add_y;

                //円の大きさ
                angle -= 5f;
                anglecount += 5f;

                if (anglecount >= 180)
                {
                    state = State.Back;
                }

            }
            else
            {
                
                
            }

            if (!IsVisible())
            {
                isDead = true;
            }
        }

        public override void Draw()
        {
            Camera.DrawGraph(x, y, Image.playerShot);
        }



        public override void OnCollision(GameObject other)
        {
            if (other is GameObject)
            {
                isDead = true;
            }
        }
    }
}
