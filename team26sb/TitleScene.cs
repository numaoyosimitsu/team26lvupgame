﻿using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class TitleScene : Scene
    {
        public TitleScene()
        {
            DX.PlayMusic("BGM/title.mp3", DX.DX_PLAYTYPE_LOOP);
            DX.SetVolumeMusic(200);
        }
        public override void Update()
        {
            if (Input.GetButtonDown(DX.PAD_INPUT_1))
            {
                Game.ChangeScene(new PlayScene(0));
            }
        }

        public override void Draw()
        {
            DX.DrawString(0, 0, "TitleSceneです。ボタン押下でPlaySceneへ。", DX.GetColor(255, 255, 255));
        }
    }
}
