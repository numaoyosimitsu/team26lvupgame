﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class Shield : GameObject
    {
        float vx;
        float vy;

        const float MoveSpeed = 10f;

        public Shield(PlayScene playScene, float x, float y, Direction direction) : base(playScene)
        {
            this.x = x;
            this.y = y;

            imageWidth = 32;
            imageHeight = 16;
            hitboxOffsetLeft = 0;
            hitboxOffsetRight = 0;
            hitboxOffsetTop = 0;
            hitboxOffsetBottom = 0;

            if (direction == Direction.Right) vx = +MoveSpeed;
            if (direction == Direction.Left) vx = -MoveSpeed;
            if (direction == Direction.Up) vy = -MoveSpeed;
            if (direction == Direction.Down) vy = +MoveSpeed;
        }

        public override void Update()
        {
            //float angleToPlayer = MyMath.PointToPointAngle(x, y, playScene.player.x, playScene.player.y - 15);
            //float speed = 3f;
            //x += (float)Math.Cos(angleToPlayer) * speed;
            //y += (float)Math.Sin(angleToPlayer) * speed;


        }


        public override void Draw()
        {
            Camera.DrawGraph(x, y, Image.shield);
        }


        public override void OnCollision(GameObject other)
        {

        }
    }
}
