﻿using DxLibDLL;
using MyLib;

namespace LVupGame
{

    public class GameOverScene : Scene
    {
        public GameOverScene()
        {
           
                DX.PlayMusic("BGM/GAME OVER.mp3", DX.DX_PLAYTYPE_LOOP);
                DX.SetVolumeMusic(200);
        }

        public override void Update()
        {
            if (Input.GetButtonDown(DX.PAD_INPUT_1))
            {
                Game.ChangeScene(new TitleScene());
            }
        }
        public override void Draw()
        {
            DX.DrawString(0, 0, "GameOverSceneです。ボタン押下でTitleSceneへ。", DX.GetColor(255, 255, 255));
        }
    }
}

