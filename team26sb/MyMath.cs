﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLib
{
    public static class MyMath
    {

        public const float Sqrt2 = 1.41421356237f;

        public static bool RectRectIntersect(
            float aLeft, float aTop, float aRight, float aBottom,
            float bLeft, float bTop, float bRight, float bBottom)
        {
            return
                aLeft < bRight &&
                aRight > bLeft &&
                aTop < bBottom &&
                aBottom > bTop;
        }

        public static float PointToPointAngle(float fromX, float fromY, float toX, float toY)
        {
            return (float)Math.Atan2(toY - fromY, toX - fromX);
        }

        public static bool SphereSphereIntersection(
            float x1, float y1, float radius1,
            float x2, float y2, float radius2)
        {
            return ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
                < ((radius1 + radius2) * (radius1 + radius2));
        }

    }
}
