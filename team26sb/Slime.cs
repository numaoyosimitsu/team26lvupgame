﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class Slime : GameObject
    {
        enum State
        {
            Normal,
            Swoon,
        }

        const int MutekiJikan = 120;

        int mutekiTimer = 0;


        public Slime(PlayScene playScene, float x, float y) : base(playScene)
        {
            this.x = x;
            this.y = y;

            life = 2;
            Exp = 1;

            imageWidth = 32;
            imageHeight = 32;
            hitboxOffsetLeft = 0;
            hitboxOffsetRight = 0;
            hitboxOffsetTop = 0;
            hitboxOffsetBottom = 0;

            playScene.enemyNum += 1;
        }

        public override void Update()
        {
            //float angleToPlayer = MyMath.PointToPointAngle(x, y, playScene.player.x, playScene.player.y);
            //float speed = 1.5f;
            //x += (float)Math.Cos(angleToPlayer) * speed;
            //y += (float)Math.Sin(angleToPlayer) * speed;
        }

        public override void Draw()
        {
            Camera.DrawGraph(x, y, Image.Slime);
        }
        public override void OnCollision(GameObject other)
        {
            if (other is Playersrash) // 相手がPlayerShotクラスなら
            {
                TakeDamage();
            }
            if (mutekiTimer <= 0)
            {
                TakeDamage();
            }

            void TakeDamage()
            {
                life -= playScene.Playrepower;

                if (life <= 0)
                {
                    isDead = true;
                    EnemyDie();
                }
                else
                {
                    mutekiTimer = MutekiJikan;
                }
            }
        }
        public void EnemyDie()
        {
            playScene.playerEXP += 1;
            playScene.enemyNum -= 1;
        }
    }
}