﻿using DxLibDLL;

namespace LVupGame
{
    public static class Image
    {
        public static int shiitake;
        public static int playerShot;
        public static int pacchi;
        public static int kamatoo;
        public static int Eye;
        public static int Slime;
        public static int Oak;
        public static int shield;
        public static int[] Player = new int[12];
        public static int[] Sword = new int[6];
        public static int[] SrashEffect = new int[24];
        public static int[] mapchip = new int[128];

        public static void Load()
        {
            shiitake = DX.LoadGraph("Image/shiitake.png");
            playerShot = DX.LoadGraph("Image/player_shot.png");
            pacchi = DX.LoadGraph("Image/pacchi_body.png");
            kamatoo = DX.LoadGraph("Image/kamatoo_32.png");
            Slime = DX.LoadGraph("Image/mon_002.png");
            Oak = DX.LoadGraph("Image/mon_102.png");
            Eye = DX.LoadGraph("Image/mon_105.png");
            shield = DX.LoadGraph("Image/shield.png");
            DX.LoadDivGraph("Image/Sword.png", Sword.Length, 6, 1, 64, 64, Sword);
            DX.LoadDivGraph("Image/Player.png", Player.Length, 3, 4, 32, 32, Player);
            DX.LoadDivGraph("Image/mapchip.png", mapchip.Length, 16, 8, 32, 32, mapchip);
            DX.LoadDivGraph("Image/SrashEffect.png", SrashEffect.Length, 6, 4, 64, 64, SrashEffect);
        }
    }
}
