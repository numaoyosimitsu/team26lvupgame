﻿using System.Collections.Generic;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class PlayScene : Scene
    {
        public enum State
        {
            Active,
            PlayerDied,
            FloorClear,
            StageClear,
        }
        public Direction NearPlayer;
        
        public Map map;
        public Player player;
        public List<GameObject> gameObjects = new List<GameObject>();
        public List<EnemyBullet> enemyBullets = new List<EnemyBullet>();
        public List<Srasheffect> srasheffects = new List<Srasheffect>();
        public State scenestate = State.Active;
        int TimerGeameover = 120; //ゲームオーバーになるまでの時間
        int TimerGeameClear = 180; //ゲームクリアになるまでの時間
        public float playerEXP = 0;
        public int enemyNum; // フロアの残りの敵の数
        private int floorClearCounter = 0; // クリア時の演出のためのカウンタ
        public float Playrepower = 0;
        // 今何番目の面なのか
        private int stageIndex;
        // 面のファイル名
        private string[] stageName = {
            "stage1",
            "stage2",
            "stage3",
            "stage4", //無いからエラー出る
            "stage5",
            "stage6",
        };
       
        public PlayScene(int stageIndex)
        {
            this.stageIndex = stageIndex;
            map = new Map(this, stageName[stageIndex]);

            player = new Player(this, 100, 100);
            Camera.LookAt(player.x, player.y);

            gameObjects.Add(player);
            //gameObjects.Add(new Slime(this, 32 * 8, 32 * 13));

            //gameObjects.Add(new Eye(this, 28 * 4, 28 * 9));

            //gameObjects.Add(new Oak(this, 36 * 8, 36 * 13));
            srasheffects = new List<Srasheffect>();

            enemyBullets = new List<EnemyBullet>();

            DX.PlayMusic("BGM/ダンジョン曲.mp3", DX.DX_PLAYTYPE_LOOP);
            DX.SetVolumeMusic(200);

            Sound.Load();

        }


        public override void Update()
        {
            if (scenestate == State.Active)
            {
                foreach (EnemyBullet enemyBullet in enemyBullets)
                {
                    if (enemyBullet.isDead)
                        break;

                    if (enemyBullet.isDead)
                        continue;

                    if (MyMath.SphereSphereIntersection(
                        player.x, player.y, player.collisionRadius,
                        enemyBullet.x, enemyBullet.y, enemyBullet.collisionRadius))
                    {
                        player.OnCollisionEnemyBullet(enemyBullet);
                    }
                }
               
                int gameObjectsCount = gameObjects.Count; // ループ前の個数を取得しておく
                for (int i = 0; i < gameObjectsCount; i++)
                {
                    gameObjects[i].Update();
                }

                for (int i = 0; i < gameObjects.Count; i++)
                {
                    GameObject a = gameObjects[i];

                    for (int j = i + 1; j < gameObjects.Count; j++)
                    {
                        if (a.isDead) break;

                        GameObject b = gameObjects[j];

                        if (b.isDead) continue;

                        if (MyMath.RectRectIntersect(
                            a.GetLeft(), a.GetTop(), a.GetRight(), a.GetBottom(),
                            b.GetLeft(), b.GetTop(), b.GetRight(), b.GetBottom()))
                        {
                            a.OnCollision(b);
                            b.OnCollision(a);
                        }
                    }
                    NearPlayer = player.player;
                    Playrepower = player.Power;
                    foreach (Srasheffect e in srasheffects)
                    {
                        e.Update();
                    }
                    foreach (EnemyBullet b in enemyBullets)
                    {
                        b.Update();
                    }
                    
                    
                }

                gameObjects.RemoveAll(go => go.isDead);

                enemyBullets.RemoveAll(eb => eb.isDead);
                srasheffects.RemoveAll(e => e.isDead);

                if (enemyNum <= 0)
                {
                    // 敵全滅
                    scenestate = State.FloorClear;
                }
            }
            else if (scenestate == State.FloorClear)
            {
                // クリア時の演出
                floorClearCounter++;
                if(stageIndex == 2)
                {
                    scenestate = State.StageClear;
                }


                // 何秒か待ってから次のシーンへ
                if (floorClearCounter >= 60 * 3)
                {
                    Game.ChangeScene(new PlayScene(stageIndex + 1));
                }
            }
            else if (scenestate == State.StageClear)
            {
                TimerGeameClear--;
                if (TimerGeameClear <= 0)
                {
                    Game.ChangeScene(new GameClearScene());
                }
            }
            else if (scenestate == State.PlayerDied)
            {
                TimerGeameover--;
                if (TimerGeameover <= 0)
                {
                    Game.ChangeScene(new GameOverScene());
                }
            }

            Camera.LookAt(player.x, player.y);
        }

        public override void Draw()
        {
            map.DrawTerrain();


            foreach (GameObject go in gameObjects)
            {
                go.Draw();
            }

            foreach (GameObject go in gameObjects)
            {
                go.DrawHitBox();
            }
            foreach (EnemyBullet b in enemyBullets)
            {
                b.Draw();
            }
            

        }
    }
}
