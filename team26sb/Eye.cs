﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    class Eye : GameObject
    {
        enum State
        {
            Normal,
            Swoon,
        }
        const int MutekiJikan = 120;

        int mutekiTimer = 0;
        int counter = 0;


        public Eye(PlayScene playScene, float x, float y) : base(playScene)
        {
            this.x = x;
            this.y = y;

            life = 2;
            Exp = 1;

            imageWidth = 32;
            imageHeight = 32;
            hitboxOffsetLeft = 0;
            hitboxOffsetRight = 0;
            hitboxOffsetTop = 0;
            hitboxOffsetBottom = 0;

            playScene.enemyNum += 1;
        }

        public override void Update()
        {
            counter++;

            if (counter % 60 == 0)
            {
                float angle = MyMath.PointToPointAngle(x, y, playScene.player.x, playScene.player.y);
                playScene.enemyBullets.Add(new EnemyBullet(x, y, angle, 1.5f));

            }
        }

        public override void Draw()
        {
            Camera.DrawGraph(x, y, Image.Eye);
        }
        public override void OnCollision(GameObject other)
        {
            if (other is Playersrash) // 相手がPlayerShotクラスなら
            {
                TakeDamage();
            }
            if (mutekiTimer <= 0)
            {
                TakeDamage();
            }

            void TakeDamage()
            {
                life -= playScene.Playrepower; // ライフ減少

                if (life <= 0)
                {
                    // ライフが無くなったら死亡
                    isDead = true;
                    EnemyDie();
                }
                else
                {
                    // 無敵時間発動
                    mutekiTimer = MutekiJikan;
                }
            }
        }
        





        public void EnemyDie()
        {
            playScene.playerEXP += 1;
            playScene.enemyNum -= 1;
        }
    }
}