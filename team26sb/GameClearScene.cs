﻿
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyLib;
    using DxLibDLL;

    namespace LVupGame
    {
        class GameClearScene : Scene
        {
        public GameClearScene()
        {

        }
            public override void Update()
            {
                if (Input.GetButtonDown(DX.PAD_INPUT_1))
                {
                    Game.ChangeScene(new TitleScene());
                }
            }
            public override void Draw()
            {
                DX.DrawString(0, 0, "GameClearSceneです。ボタン押下でTitleSceneへ。", DX.GetColor(255, 255, 255));
            }
        }
    }

