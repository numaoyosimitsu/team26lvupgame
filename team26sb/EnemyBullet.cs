﻿using System;
using DxLibDLL;
using MyLib;

namespace LVupGame
{
    public class EnemyBullet
    {
        const float VisibleRadius = 8f; // 見た目の半径

        public float x; // x座標
        public float y; // y座標
        public float collisionRadius = 16f;
        public bool isDead = false; // 死亡フラグ

        float vx; // x方向移動速度
        float vy; // y方向移動速度

        public EnemyBullet(float x, float y, float angle, float speed)
        {
            this.x = x;
            this.y = y;

            // 角度からx方向の移動速度を算出
            vx = (float)Math.Cos(angle) * speed;
            // 角度からy方向の移動速度を算出
            vy = (float)Math.Sin(angle) * speed;
        }


        // 更新処理
        public void Update()
        {
            // 速度の分だけ移動
            x += vx;
            y += vy;

            // 画面外に出たら死亡フラグを立てる
            if (y + VisibleRadius < -10 || y - VisibleRadius > Screen.Height + 80 ||
                x + VisibleRadius < 0 || x - VisibleRadius > Screen.Width + 50)
            {
                isDead = true;
            }

        }

        // 描画処理
        public void Draw()
        {
            Camera.DrawGraph(x, y, Image.playerShot);
        }

        public void OnCollision(EnemyBullet other)
        {

        }

        public void OnCollisionEnemy(Player player)
        {
            isDead = true;
        }
    }
}
